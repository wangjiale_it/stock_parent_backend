package com.itheima.stock.mapper;

import com.itheima.stock.entity.SysRole;

/**
* @author Administrator
* @description 针对表【sys_role(角色表)】的数据库操作Mapper
* @createDate 2024-07-19 16:48:40
* @Entity com.itheima.stock.entity.SysRole
*/
public interface SysRoleMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysRole record);

    int insertSelective(SysRole record);

    SysRole selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysRole record);

    int updateByPrimaryKey(SysRole record);

}
