package com.itheima.stock.mapper;

import com.itheima.stock.entity.StockMarketIndexInfo;
import com.itheima.stock.pojo.domain.InnerMarketDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
* @author Administrator
* @description 针对表【stock_market_index_info(国内大盘数据详情表)】的数据库操作Mapper
* @createDate 2024-07-19 16:48:40
* @Entity com.itheima.stock.entity.StockMarketIndexInfo
*/
public interface StockMarketIndexInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockMarketIndexInfo record);

    int insertSelective(StockMarketIndexInfo record);

    StockMarketIndexInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockMarketIndexInfo record);

    int updateByPrimaryKey(StockMarketIndexInfo record);

    List<InnerMarketDomain> getMarketInfo(@Param("markerIds") List<String> marketIds);

    List<Map> getStockTradeVol(@Param("markerIds") List<String> markedIds, @Param("openTime") String openTime, @Param("endTime") String curTime);
}
