package com.itheima.stock.mapper;

import com.itheima.stock.entity.SysLog;

/**
* @author Administrator
* @description 针对表【sys_log(系统日志)】的数据库操作Mapper
* @createDate 2024-07-19 16:48:40
* @Entity com.itheima.stock.entity.SysLog
*/
public interface SysLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(SysLog record);

    int insertSelective(SysLog record);

    SysLog selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(SysLog record);

    int updateByPrimaryKey(SysLog record);

}
