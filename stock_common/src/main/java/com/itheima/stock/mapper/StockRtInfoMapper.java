package com.itheima.stock.mapper;

import com.itheima.stock.entity.StockRtInfo;
import com.itheima.stock.pojo.domain.Stock4MinuteDomain;
import com.itheima.stock.pojo.domain.StockUpdownDomain;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
* @author Administrator
* @description 针对表【stock_rt_info(个股详情信息表)】的数据库操作Mapper
* @createDate 2024-07-19 16:48:40
* @Entity com.itheima.stock.entity.StockRtInfo
*/
public interface StockRtInfoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(StockRtInfo record);

    int insertSelective(StockRtInfo record);

    StockRtInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(StockRtInfo record);

    int updateByPrimaryKey(StockRtInfo record);

    List<StockUpdownDomain> getNewestStockInfo();

    List<Map> getStockUpdownCount(@Param("openTime") String openTime, @Param("curTime") String curTime, @Param("flag") int flag);

    List<Map> getStockUpDownSectionByTime(@Param("startTime") String openTime);

    List<Stock4MinuteDomain> getStockInfoByCodeAndDate(@Param("stockCode") String code, @Param("startTime") String openTime, @Param("endTime") String curTime);
}
