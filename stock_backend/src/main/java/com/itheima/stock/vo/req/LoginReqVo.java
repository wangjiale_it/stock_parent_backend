package com.itheima.stock.vo.req;

import lombok.Data;

@Data
public class LoginReqVo {

    private String username;

    private String password;

    private String code;

    /**
     * 保存redis随机码的key，也就是sessionId
     */
    private String sessionId;
}
