package com.itheima.stock.vo.resp;

import com.github.pagehelper.PageInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 */
@Data
public class PageResult<T> implements Serializable {

    private Long totalRows;

    private Integer totalPages;

    private Integer pageNum;

    private Integer pageSize;


    private Integer size;

    private List<T> rows;


    /**
     * 分页数据组装
     * @param pageInfo
     * @return
     */
    public PageResult(PageInfo<T> pageInfo) {
        this.totalRows = pageInfo.getTotal();
        this.totalPages = pageInfo.getPages();
        this.pageNum = pageInfo.getPageNum();
        this.pageSize = pageInfo.getPageSize();
        this.size = pageInfo.getSize();
        rows = pageInfo.getList();
    }
}
