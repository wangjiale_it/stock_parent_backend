package com.itheima.stock.vo.resp;

import lombok.Data;

@Data
public class LoginRespVo {

    private String phone;

    private String username;

    private String nickName;
}
